#include <stdlib.h>
#include <stdio.h>
#include <readline/readline.h>
#include <wordexp.h>

#define PROMPT "# "

int main()
{
    char* input_str;
    wordexp_t cmd;

    if(!(input_str = readline(PROMPT))) {    // command input
        printf("exit\n");
        exit(0);
    }

    if(wordexp(input_str, &cmd, WRDE_SHOWERR | WRDE_UNDEF))
    {
        printf("error\n");
        exit(0);
    }

    for (int i = 0; i < cmd.we_wordc; i++)
        printf("|%s|", cmd.we_wordv[i]);

    printf("\n");
    wordfree(&cmd);
}
