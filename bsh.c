#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <wordexp.h>

/*
 * Includes agregados por ExiLio
 *
 */
#include <signal.h>
#include <dirent.h>
#include <sys/types.h>


#define PROMPT "# "
#define NELEMS(x)  (sizeof(x) / sizeof(x[0]))

const char* BUILT_IN[] = {"","exit","mycd","mypwd","export","myls","concr"};

int cmd_prompt(char*, wordexp_t*);
void free_input(char*, wordexp_t*);
void exec_builtin(int, char*[]);
void exec_cmd(char*[]);
int is_builtin(char*[]);

/*
 * Funciones agregadas
 * by ExiLio
 */
void captured_signal(int n);
void myls_function();
char* get_prompt();
void exec_concr(char* cmd_argv[]);
void concurrente(char*[]);
void concr_exec (char**);
void formatConcr(char**,char**);
int es_myAmpersand(char**);


/*
 * Function: main
 * --------------
 * Main command loop.
 */
int main(int argc, char* argv[])
{
    char* input_str = NULL;
    int cmd = 0;
    wordexp_t cmd_argv;

    while(1)
    {

        //Captura de señales
        signal(SIGINT , &captured_signal); //Captura de la señal CTRL+C


        if(!cmd_prompt(input_str, &cmd_argv))
        {
            free_input(input_str, &cmd_argv);
            continue;
        }

        if ((cmd = is_builtin(cmd_argv.we_wordv)))
            exec_builtin(cmd, cmd_argv.we_wordv);   // exec built_in command
        else
            exec_cmd(cmd_argv.we_wordv);            // exec command

        free_input(input_str, &cmd_argv);
    }
}

/*
 * Function:  cmd_prompt
 * -----------------------
 * Handles command line input using readline and wordexp.
 * Prints the prompt and waits for user's input returning 0 if no command was
 * issued, or 1 otherwise with cmd_argv holding the parsed command line.
 */
int cmd_prompt(char* input_str, wordexp_t* cmd_argv)
{
    if(!(input_str = readline(get_prompt())))     // command input
    {

        printf("exit\n");
        exit(0);
    }

    int error = wordexp(input_str, cmd_argv, WRDE_SHOWERR | WRDE_UNDEF);

    if(error)
    {
        switch (error) {
            case WRDE_BADCHAR:
                printf("Bad character. Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }\n");
                break;
            case WRDE_BADVAL:
                printf("Undefined variable\n");
                break;
            case WRDE_SYNTAX:
                printf("Syntax error\n");
                break;
            default:
                printf("Unknown error\n");
        }

         return 0;
    }

    if (cmd_argv->we_wordc == 0) // no input
    {
        return 0;
    }

    add_history(input_str);

    return 1;
}

/*
 * Function: exec_cmd
 * ------------------
 * Execute given command and waits till it's completed. cmd_argv[0] must shields
 * the command to be executed, cmd_argv[i] (i > 0) shields the command arguments.
 */
void exec_cmd(char* cmd_argv[])
{
    int status;
    int child_pid = fork();

    if (child_pid != 0) // parent process
    {
        waitpid(child_pid, &status, 0);
    }
    else // child process
    {
        execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
        perror("");
        exit(0);
    }
}

/*
 * Function: exec_builtin
 * ----------------------
 * Executes built-in commmands registered in BUILT_IN array.
 */
void exec_builtin(int cmd, char* cmd_argv[])
{
    char path[255];

    switch(cmd)
    {
        case 1:
          exit(0);
          break;
        case 2:
          chdir(cmd_argv[1]);
          break;
        case 3:
          getcwd(path, 255);
          printf("%s\n", path);
          break;
        case 4:
    		  setenv(cmd_argv[1],cmd_argv[2],0);
    			break;
    		case 5:
    			myls_function();
    			break;
	      case 6:
    	    concr_exec(cmd_argv);
    	    break;
        default:
            printf("-bsh: command not found\n");
    }

    return;
}

/*
 * Function: is_builtin
 * ----------------------
 * Finds out if the command is a built-in one, registered in BUILT_IN array and
 * returns the command position.
 */
int is_builtin(char* cmd_argv[])
{
    for(int i = 1; i < NELEMS(BUILT_IN); i++)
        if (!strcmp(cmd_argv[0], BUILT_IN[i]))
            return i;

    return 0;
}


// utility functions
void free_input(char* cmd_str, wordexp_t* cmd_argv)
{
    free(cmd_str);
    wordfree(cmd_argv);
}

/*
 * Agregados por ExiLio
 *
 */

void captured_signal(int n) {
	printf("Señal CTRL+C atrapada\n");
}

void myls_function() {

	DIR *dirp;
	struct dirent *direntp;
	dirp = opendir(".");
	if (dirp == NULL) {
		printf("Error: no se puede abrir el archivo");
	}else {
		while ((direntp = readdir(dirp)) != NULL) {
			printf("%s\n", direntp->d_name);
 		}
	}
	closedir(dirp);
}

char* get_prompt() {
	char path[255];
  getcwd(path,255);
	return ("%s  " , path);
}

void exec_concr(char* cmd_argv[])
{
	fork();
	execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
    perror("");
    exit(0);
}

void concurrente(char* cmd_argv[]) {

    int child_pid = fork();

    if (!child_pid != 0) { // parent process
	execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
    }
}

void concr_exec (char* cmd_argv[]) {
	int tamanioDelPene = sizeof(cmd_argv);
	char* toExecute[tamanioDelPene];
	int cmd = 0;

	formatConcr(cmd_argv,toExecute);

	if ((cmd = is_builtin(toExecute))) {
		exec_builtin(cmd,toExecute);
	} else {
		concurrente(toExecute);
	}
}

void formatConcr(char* cmd_argv[],char* toExecute[]) {
	for (int i=0 ; i<sizeof(cmd_argv)-1 ; i++) {
		toExecute[i] = cmd_argv[i+1];
	}
	//toExecute[k] = NULL;
}

int es_myAmpersand(char* cmd_argv[]) {
	int res = 0;
	int pos=0;
	while (!res || pos > sizeof(cmd_argv)) {
		if (cmd_argv[pos] == "amper") {
			res = 1;
		}
	}
	return res;
}
